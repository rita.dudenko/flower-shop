import "@babel/polyfill";
import express from "express";
import path from "path";
// import CONFIG from "./config/index";
// import appLoader from "./loaders/index";


const startServer =  () => {
  const app = express();

  // await appLoader(app);

  app.use("/", express.static(path.join(__dirname, "../frontend/build")));

  app.listen(process.env.PORT || 3005, () => {
    console.log(`############## Server has started on port ${process.env.PORT}! ##############`);
  });

};

startServer();